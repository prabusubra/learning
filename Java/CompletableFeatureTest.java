import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Beta {
    public static void main(String[] args) {
        long istart = System.currentTimeMillis();
        HttpClient client = HttpClient.newHttpClient();
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://jsonplaceholder.typicode.com/photos"))
                    .header("Content-Type", "application/json").build();
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());

            System.out.println(response.body());
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://jsonplaceholder.typicode.com/posts"))
                    .header("Content-Type", "application/json").build();
            HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());

            System.out.println(response.body());
        } catch (Exception e){
            e.printStackTrace();
        }

        long iend = System.currentTimeMillis();
        System.out.println("(iend-istart) = " + (iend-istart)); //1371
    }
}

/***********/

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class Gamma {
    public static void main(String[] args)  throws Exception{
        long stime = System.currentTimeMillis();
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://jsonplaceholder.typicode.com/photos")).build();
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).thenApply(HttpResponse::body).thenAccept(System.out::println);

        HttpRequest request1 = HttpRequest.newBuilder().uri(URI.create("https://jsonplaceholder.typicode.com/posts")).build();
        client.sendAsync(request1, HttpResponse.BodyHandlers.ofString()).thenApply(HttpResponse::body).thenAccept(System.out::println);
        long etime = System.currentTimeMillis();

        Thread.sleep(1000);

        System.out.println("(etime - stime) = " + (etime - stime));
    }
}
/*****************/

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

public class Delta {
    public static void main(String[] args) throws Exception{
        long istart = System.currentTimeMillis();
        HttpClient client = HttpClient.newHttpClient();
        CompletableFuture.supplyAsync(()->{
            HttpResponse response = null;
            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://jsonplaceholder.typicode.com/photos"))
                        .header("Content-Type", "application/json").build();
                response = client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (Exception e){
                e.printStackTrace();
            }
            return response.body();
        }).thenAccept(data ->{
            System.out.println("data 1= " + data);
        });

        CompletableFuture.supplyAsync(()->{
            HttpResponse response = null;
            try {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create("https://jsonplaceholder.typicode.com/posts"))
                        .header("Content-Type", "application/json").build();
                response = client.send(request, HttpResponse.BodyHandlers.ofString());

            } catch (Exception e){
                e.printStackTrace();
            }
            return response.body();
        }).thenAccept(data ->{
            System.out.println("data 2 =  " + data);
        });
        long iend = System.currentTimeMillis();
        Thread.sleep(1000);
        System.out.println("(iend-istart) = " + (iend-istart)); //326
    }
}


